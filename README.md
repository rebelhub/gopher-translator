# README #

### What is this repository for? ###

* Quick summary

A simple gopher translation service for translating english words, sentences (multiple ones) into gopher language. All in all, a test for Xoomworks.
  
* Version
0.1


### How do I get set up? ###
If you have docker, just do `docker-compose up`  in the main folder and you're up and ready.

* Configuration
  
  The app will run on port **8080** by default, but you can change that via the `-port=XXXX` flag if you run it manually, or via the `.env` file.
  In case you *do* change it, don't forget to add the command to the Dockerfile CMD section, and/or change the ports in the `docker-compose.yml`
  
* Dependencies
  
None! Although in any other case I'd probably go straight for gORM, fiber and/or gin, I figured it'd be best to keep it as vanilla as possible.

* Demo
After running the server, hit 0.0.0.0:PORT ( usually [http://0.0.0.0:8080](http://0.0.0.0:8080) ), in order to test through a UI.

* Deployment instructions

Just compile as usual. Keep in mind that the .env is gitignored, so make sure you create a PRODUCTION ready .env before deployment.


### Who do I talk to? ###

* Repo owner or admin
  
Yanko Ivanov <yanko.a.ivanov@gmail.com>