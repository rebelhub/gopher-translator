package types

type Translatable interface {
	Translate(word []Word) string
}
