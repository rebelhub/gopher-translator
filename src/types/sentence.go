package types

import "strings"

type Sentence struct {
	words    []Word
	Content  string
	EndsWith string
}

func (sentence *Sentence) Translate() string {

	sentence.parse()

	var translatedWords []string
	for i := range sentence.words {
		translatedWords = append(translatedWords, sentence.words[i].Translate())
	}

	return strings.Join(translatedWords, " ") + sentence.EndsWith + " "
}

/**
Fills out the words array, in preparation for translation
*/
func (sentence *Sentence) parse() {

	// Split 'em out
	wordStrings := strings.Split(sentence.Content, " ")

	for i := range wordStrings {
		sentence.words = append(sentence.words, Word{
			Content: wordStrings[i],
		})
	}
}
