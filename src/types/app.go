package types

type App struct {
	history *History
}

func (app *App) Init() {
	app.history = &History{}
}

func (app *App) GetHistoryService() *History {
	return app.history
}
