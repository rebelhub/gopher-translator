package types

import "strings"

type Word struct {
	Content string `json:"content"`
}

func (word Word) Translate() string {

	var resultingWord = word.Content
	if word.startsWithVowel() {
		resultingWord = "g" + word.Content
	}

	if word.Content[0:2] == "xr" || word.Content[0:2] == "XR" {
		resultingWord = "ge" + resultingWord
	} else if word.startsWithConsonant() {
		consonantSound := word.sayConsonantSound()

		// Strip the sound, add it to the end and add "ogo"
		resultingWord = strings.Replace(resultingWord, consonantSound, "", 1) + consonantSound + "ogo"
	}

	return resultingWord
}

func (word Word) startsWithVowel() bool {
	return isVowel(word.Content[0:1])
}

func isVowel(char string) bool {
	switch char {
	case "a", "e", "i", "o", "u", "y",
		"A", "E", "I", "O", "U", "Y":
		return true
	default:
		return false
	}
}

func (word Word) startsWithConsonant() bool {
	return isConsonant(word.Content[0:1])
}

func isConsonant(char string) bool {
	switch char {
	case "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "y", "z",
		"B", "C", "D", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W", "X", "Y", "Z":
		return true
	default:
		return false
	}
}

func (word Word) sayConsonantSound() string {

	sound := ""
	for i := range word.Content {
		str := string(word.Content[i])
		if isConsonant(str) {
			sound += str
		} else {

			/*
				Special 4th case essentially states that "u" is treated as a
				part of the consonant sound, so add it and break
				afterwards as "u" is a vowel anyway
			*/
			if str == "u" || str == "U" {
				sound += str
			}

			// Stop when you find a vowel
			break
		}

	}

	return sound
}
