package types

import (
	"sort"
)

type History struct {
	words   map[string]string
	sortMap []string
}

func (history *History) Get() []map[string]string {

	var h []map[string]string

	for _, val := range history.sortMap {
		h = append(h, map[string]string{
			val: history.words[val],
		})
	}

	return h
}

func (history *History) Add(english string, gopher string) {

	if len(history.words) == 0 {
		history.words = make(map[string]string)
	}

	history.words[english] = gopher

	// Sort in place when adding a new history item, so that we don't have to do it on every request
	// We're assuming that Read operations are more than the Create operations for the API
	// as is usually the case. Otherwise, we can just move the sort to the Get() method
	history.updateSortMap()
}

func (history *History) updateSortMap() {
	history.sortMap = nil

	var keys []string
	for key, _ := range history.words {
		keys = append(keys, key)
	}

	sort.Strings(keys)

	history.sortMap = keys
}
