package main

import (
	"flag"

	"html/template"
	"log"
	"main/handlers"
	"main/types"
	"net/http"
	"os"
)

func main() {

	app := types.App{}
	app.Init()

	EnvPort, portVarExists := os.LookupEnv("PORT")
	if portVarExists {
		EnvPort = os.Getenv("PORT")
	} else {
		EnvPort = "8080"
	}

	port := flag.String("port", EnvPort, "The port on which to expose the translator")
	flag.Parse()

	http.HandleFunc("/word", func(writer http.ResponseWriter, request *http.Request) {
		handlers.Word(writer, request, &app)
	})

	http.HandleFunc("/sentence", func(writer http.ResponseWriter, request *http.Request) {
		handlers.Sentence(writer, request, &app)
	})

	http.HandleFunc("/history", func(writer http.ResponseWriter, request *http.Request) {
		handlers.History(writer, request, &app)
	})

	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		tmpl := template.Must(template.ParseFiles("../public/index.html"))

		err := tmpl.Execute(writer,
			struct {
				EnvPort string
			}{EnvPort})

		if err != nil {
			http.Error(writer, http.StatusText(500), 500)
		}
	})

	println("Server running on http://0.0.0.0:" + *port)
	log.Fatal(http.ListenAndServe(":"+*port, nil))
}
