package handlers

import (
	"encoding/json"
	"main/types"
	"net/http"
	"strings"
)

type SentenceRequestForm struct {
	EnglishSentence string `json:"english-sentence"`
}

type SentenceResponseForm struct {
	GopherSentence string `json:"gopher-sentence"`
}

func Sentence(resp http.ResponseWriter, req *http.Request, app *types.App) {

	resp.Header().Set("Access-Control-Allow-Origin", "*")
	resp.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

	if req.Method == "OPTIONS" {
		return
	}

	if req.Method != "POST" {
		http.Error(resp, "", http.StatusBadRequest)
	}

	resp.Header().Set("Content-Type", "application/json")

	reqF := SentenceRequestForm{}
	err := json.NewDecoder(req.Body).Decode(&reqF)

	if err != nil {
		http.Error(resp, err.Error(), http.StatusInternalServerError)
	}

	var sentences []types.Sentence
	var temp string
	for i := range reqF.EnglishSentence {
		el := string(reqF.EnglishSentence[i])
		switch el {
		case "!", "?", ".":

			// this usually means !! ?? or .., which we'll treat as gibberish gophers altogether
			if len(temp) == 0 {
				continue
			}
			sentences = append(sentences, types.Sentence{
				Content:  strings.TrimSpace(temp),
				EndsWith: el, // we have to remember how the sentence ends, so we can patch it up after the translation
			})
			temp = ""
		default:
			temp += el
		}
	}

	// If there are no sentence separators, we'll treat the whole thing as one sentence
	if len(sentences) == 0 {
		sentences = append(sentences, types.Sentence{
			Content:  reqF.EnglishSentence,
			EndsWith: "",
		})
	}

	resF := SentenceResponseForm{}

	for i := range sentences {
		resF.GopherSentence += sentences[i].Translate()
	}

	resF.GopherSentence = strings.TrimSpace(resF.GopherSentence)

	history := app.GetHistoryService()
	history.Add(reqF.EnglishSentence, resF.GopherSentence)

	enc, err := json.Marshal(resF)

	if err != nil {
		http.Error(resp, err.Error(), http.StatusInternalServerError)
		return
	}

	_, err = resp.Write(enc)

	if err != nil {
		http.Error(resp, err.Error(), http.StatusInternalServerError)
		return
	}
}
