package handlers

import (
	"encoding/json"
	"main/types"
	"net/http"
)

type HistoryResponseForm struct {
	History []map[string]string `json:"history"`
}

func History(resp http.ResponseWriter, req *http.Request, app *types.App) {

	resp.Header().Set("Access-Control-Allow-Origin", "*")
	resp.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

	if req.Method == "OPTIONS" {
		return
	}

	if req.Method != "GET" {
		http.Error(resp, "", http.StatusBadRequest)
	}

	resp.Header().Set("Content-Type", "application/json")

	resF := HistoryResponseForm{}

	history := app.GetHistoryService()
	resF.History = history.Get()

	enc, err := json.Marshal(resF)

	if err != nil {
		http.Error(resp, err.Error(), http.StatusInternalServerError)
		return
	}

	_, err = resp.Write(enc)

	if err != nil {
		http.Error(resp, err.Error(), http.StatusInternalServerError)
		return
	}
}
