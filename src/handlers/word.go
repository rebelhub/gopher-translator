package handlers

import (
	"encoding/json"
	"main/types"
	"net/http"
)

type WordRequestForm struct {
	EnglishWord string `json:"english-word"`
}

type WordResponseForm struct {
	GopherWord string `json:"gopher-word"`
}

func Word(resp http.ResponseWriter, req *http.Request, app *types.App) {

	resp.Header().Set("Access-Control-Allow-Origin", "*")
	resp.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

	if req.Method == "OPTIONS" {
		return
	}

	if req.Method != "POST" {
		http.Error(resp, "", http.StatusBadRequest)
	}

	resp.Header().Set("Content-Type", "application/json")

	reqF := WordRequestForm{}
	err := json.NewDecoder(req.Body).Decode(&reqF)

	if err != nil {
		panic(err)
		http.Error(resp, err.Error(), http.StatusInternalServerError)
	}

	word := types.Word{
		Content: reqF.EnglishWord,
	}

	resF := WordResponseForm{}
	resF.GopherWord = word.Translate()

	history := app.GetHistoryService()
	history.Add(reqF.EnglishWord, resF.GopherWord)

	//println("Translated word is " + word.Translate())

	enc, err := json.Marshal(resF)

	if err != nil {
		http.Error(resp, err.Error(), http.StatusInternalServerError)
		return
	}

	_, err = resp.Write(enc)

	if err != nil {
		http.Error(resp, err.Error(), http.StatusInternalServerError)
		return
	}
}
